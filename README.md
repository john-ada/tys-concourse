# ThankYouSpot Concourse
ThankYouSpot Continuous Deployment Pipelines with [Concourse](https://concourse-ci.org/docs.html).

## Instructions
1. Run `cp sample.pipeline.yml pipeline.yml`.
2. Replace the variables.
3. Install the **fly CLI** by following the instructions here: [https://concoursetutorial.com/](https://concoursetutorial.com/).
4. Install Docker.
5. Run `yarn fresh-start`.
6. Open [http://localhost:8888](http://localhost:8888) on the browser.
7. Click the **Login** button on the upper-right corner then select `Main`.

## Update
Run `yarn pipeline`.

## Troubleshooting
If the jobs aren't starting, wait for a few minutes. If it really doesn't start, run:
```sh
fly -t tys check-resource -r ThankYouSpot/tys-dev
fly -t tys check-resource -r ThankYouSpot/dev-repo-cache
```